﻿using SitefinityCalendar.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Events.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Events;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Workflow;

namespace SitefinityCalendar.Services
{
	public class EventsService : IEventsService
	{
		private readonly EventsManager _manager;

		public EventsService(EventsManager manager)
		{
			_manager = manager;
		}

		// Returns an IQueryable of all published events in the system.
		public IQueryable<Event> GetAllEvents()
		{
			var events = _manager.GetEvents()
				.Where(e => e.ApprovalWorkflowState == "Published" && e.Status != ContentLifecycleStatus.Deleted && e.Visible == true)
				.OrderBy(e => e.EventStart);

			return events;
		}

		// Returns an IQueryable of all published events that belong to a specific calendar.
		// The calendar is identified by its title.
		public IQueryable<Event> QueryEventsByCalendar(string calendarTitle)
		{
			var calendar = _manager.GetCalendars()
				.FirstOrDefault(c => c.Title == calendarTitle);

			var calendarEvents = _manager.GetCalendarEvents(calendar.Id)
				.Where(e => e.ApprovalWorkflowState == "Published" && e.Status != ContentLifecycleStatus.Deleted && e.Visible == true)
				.OrderBy(e => e.EventStart);

			return calendarEvents;
		}

		// Returns an IQueryable of all published events in the system.
		// The results are sorted based on the value of the "sortBy" parameter.
		public IQueryable<Event> QueryEvents(string sortBy)
		{
			var events = _manager.GetEvents()
				.Where(e => e.ApprovalWorkflowState == "Published" && e.Status != ContentLifecycleStatus.Deleted && e.Visible == true);

			switch (sortBy)
			{
				case "start":
					events = events.OrderBy(e => e.EventStart);
					break;
				case "end":
					events = events.OrderByDescending(e => e.EventEnd);
					break;
				default:
					events = events.OrderBy(e => e.EventStart);
					break;
			}

			return events;
		}

		// Creates ten new events in the system.
		// If the event is odd it's assigned to the first calendar and if it's even to the second.
		// If an event with the same name already exists for either calendar, it skips the create function.
		// After creating each event, it is saved and a workflow is initiated to publish it.
		public void CreateTenEvents()
		{
			var events = _manager.GetEvents();

			var calendarOne = _manager.GetCalendars()
				.FirstOrDefault(c => c.Title == "First Calendar");

			var calendarTwo = _manager.GetCalendars()
				.FirstOrDefault(c => c.Title == "Second Calendar");

			for (int i = 1; i <= 10; i++)
			{
				var eventName = "Event " + i;

				if (events.Any(e => e.Title == eventName && (e.Parent == calendarOne || e.Parent == calendarTwo)))
				{
					continue;
				}

				var startDate = DateTime.Today.AddDays(i);
				var endDate = startDate.AddDays(1);

				var ev = _manager.CreateEvent();
				ev.Title = eventName;
				ev.Content = "Random Content";
				ev.Description = "Description of event " + i;
				ev.EventStart = startDate;
				ev.EventEnd = endDate;
				ev.UrlName = "event-" + i;
				ev.Parent = startDate.Day % 2 == 0 ? calendarOne : calendarTwo;

				_manager.SaveChanges();

				var bag = new Dictionary<string, string>();
				bag.Add("ContentType", typeof(Event).FullName);
				WorkflowManager.MessageWorkflow(ev.Id, typeof(Event), null, "Publish", false, bag);
			}
		}
	}
}