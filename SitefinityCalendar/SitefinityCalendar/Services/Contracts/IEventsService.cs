﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Sitefinity.Events.Model;

namespace SitefinityCalendar.Services.Contracts
{
	public interface IEventsService
	{
		IQueryable<Event> GetAllEvents();

		IQueryable<Event> QueryEventsByCalendar(string calendarTitle);

		IQueryable<Event> QueryEvents(string sortBy);

		void CreateTenEvents();
	}
}
