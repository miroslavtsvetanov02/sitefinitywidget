﻿using Ninject.Modules;
using SitefinityCalendar.Services.Contracts;
using SitefinityCalendar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityCalendar
{
	public class InterfaceMappings : NinjectModule
	{
		public override void Load()
		{
			this.Bind<IEventsService>().To<EventsService>();
		}
	}
}