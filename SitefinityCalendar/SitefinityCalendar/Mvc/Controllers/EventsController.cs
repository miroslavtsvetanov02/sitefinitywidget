﻿using SitefinityCalendar.Mvc.Models;
using SitefinityCalendar.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Events;
using Telerik.Sitefinity.Mvc;

namespace SitefinityCalendar.Mvc.Controllers
{
	[ControllerToolboxItem(Name = "EventsWidget", Title = "Events Widget", SectionName = "Custom Widgets")]
	public class EventsController : Controller
	{
		private readonly IEventsService _eventService;
		private readonly EventsManager _manager;

		public EventsController(IEventsService eventService, EventsManager manager)
		{
			_eventService = eventService;
			_manager = manager;
		}

		// If calendarTitle is null, retrieves all events sorted by start date.
		// If calendarTitle is not null, retrieves events filtered by calendar title and sorted by start date.
		public ActionResult Index(string calendarTitle = null)
		{
			_eventService.CreateTenEvents();

			var events = string.IsNullOrEmpty(calendarTitle)
				? _eventService.GetAllEvents()
				: _eventService.QueryEventsByCalendar(calendarTitle);

			var calendars = _manager.GetCalendars();

			var result = new EventsWidgetModel()
			{
				Calendars = calendars,
				Events = events,
			};

			return View(result);
		}

		// Retrieves all events sorted by the value of sortBy parameter.
		public ActionResult Order(string sortBy)
		{
			var events = _eventService.QueryEvents(sortBy);

			var calendars = _manager.GetCalendars();

			var result = new EventsWidgetModel()
			{
				Calendars = calendars,
				Events = events,
			};

			return View("Index", result);
		}
	}
}