﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Events.Model;

namespace SitefinityCalendar.Mvc.Models
{
	public class EventsWidgetModel
	{
		public IQueryable<Calendar> Calendars { get; set; }
		public IQueryable<Event> Events { get; set; }
	}
}